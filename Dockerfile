FROM golang
ADD . /usr/src/product
WORKDIR /usr/src/product/cmd/web
CMD ["go", "run", "."]