package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-redis/redis/v8"
	"google.golang.org/grpc"
	"log"
	"n_products/greet"
	"n_products/pkg/modelss"
	"n_products/pkg/modelss/rediss"
	"net/http"
	"os"
	"strings"
)

var secret string

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
	Products *rediss.ProductDatabase
}

// setting handler routes
func (app *application) routes() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/add", TokenVerifyMiddleWare(app.add))
	mux.HandleFunc("/delete", TokenVerifyMiddleWare(app.delete))

	return mux
}

func (app *application) add(w http.ResponseWriter, r *http.Request) {
	claims, _ := app.getClaimsFormJWT(r)
	email, _ := claims["user_email"].(string)

	var myprod modelss.Product

	err := json.NewDecoder(r.Body).Decode(&myprod)
	if err != nil {
		http.Error(w, "NOT PROPER DATA", http.StatusBadRequest)
		return
	}

	err = app.Products.SetProduct(myprod, email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	conn, err := grpc.Dial("notify:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	cAdd := greet.NewNotificationServiceClient(conn)

	app.doLongGreetAdd(r, cAdd)
}

func (app *application) doLongGreetAdd(r *http.Request, c greet.NotificationServiceClient) {
	claims, _ := app.getClaimsFormJWT(r)
	email, _ := claims["user_email"].(string)

	requests := greet.MessageRequest{UserEmail: email}

	ctx := context.Background()
	_, err := c.SendEmailAdd(ctx, &requests)
	if err != nil {
		log.Fatalf("error while calling LongGreet: %v", err)
	}

}

func (app *application) delete(w http.ResponseWriter, r *http.Request) {
	claims, _ := app.getClaimsFormJWT(r)
	email, _ := claims["user_email"].(string)

	err := app.Products.EmptyProduct(email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	conn, err := grpc.Dial("notify:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	cDelete := greet.NewNotificationServiceClient(conn)

	app.doLongGreetDel(r, cDelete)
}

func (app *application) doLongGreetDel(r *http.Request, c greet.NotificationServiceClient) {
	claims, _ := app.getClaimsFormJWT(r)
	email, _ := claims["user_email"].(string)

	requests := greet.MessageRequest{UserEmail: email}

	ctx := context.Background()
	_, err := c.SendEmailDel(ctx, &requests)
	if err != nil {
		log.Fatalf("error while calling LongGreet: %v", err)
	}

}

// token validator middleware
func TokenVerifyMiddleWare(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		bearerToken := strings.Split(authHeader, " ")

		if len(bearerToken) == 2 {
			authToken := bearerToken[1]

			token, error := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}

				return []byte(secret), nil
			})

			if error != nil {
				http.Error(w, "INVALID TOKEN", http.StatusBadRequest)
				return
			}

			if token.Valid {
				next.ServeHTTP(w, r)
			} else {
				http.Error(w, "INVALID TOKEN AUTHORIZATION FAIL", http.StatusUnauthorized)
				return
			}
		} else {
			http.Error(w, "INVALID TOKEN", http.StatusBadRequest)
			return
		}
	})
}

func (app *application) getClaimsFormJWT(r *http.Request) (jwt.MapClaims, bool) {
	authHeader := r.Header.Get("Authorization")
	bearerToken := strings.Split(authHeader, " ")
	tokenStr := bearerToken[1]

	ByteSecret := []byte(secret)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return ByteSecret, nil
	})

	if err != nil {
		app.errorLog.Println(err.Error())
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		return claims, true
	} else {
		app.errorLog.Println(err.Error())
		return nil, false
	}
}

func main() {

	// get port and secret for jwt
	addr := flag.String("addr", ":8001", "HTTP network address")
	secret = *flag.String("secret", "naruto_uzumaki", "secret_for_jwt")
	flag.Parse()

	rdb := redis.NewClient(&redis.Options{
		Addr:     "redisdb:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	// architecture by book from first half
	app := &application{
		errorLog: errorLog,
		infoLog:  infoLog,
		Products: &rediss.ProductDatabase{Database: rdb},
	}

	srv := &http.Server{
		Addr:     *addr,
		ErrorLog: errorLog,
		Handler:  app.routes(),
	}

	infoLog.Printf("Starting server on %s", *addr)
	err := srv.ListenAndServe()
	errorLog.Fatal(err)
}
