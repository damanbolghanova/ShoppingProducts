package rediss

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"n_products/pkg/modelss"
	"time"
)

type ProductDatabase struct {
	Database *redis.Client
}

func (p *ProductDatabase) SetProduct(prod modelss.Product, email string) error {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	srt := p.Database.Get(ctx, email)
	if srt.Err() == redis.Nil {
		jsons, err := json.Marshal(prod)
		if err != nil {
			return err
		}
		err = p.Database.Set(ctx, email, jsons, 0).Err()
		if err != nil {
			return err
		}
	} else {
		var data modelss.Products
		err := json.Unmarshal([]byte(srt.Val()), &data)
		if err != nil {
			fmt.Println("Error")
			return err
		}

		data.AllProds = append(data.AllProds, prod)
		jsons, err := json.Marshal(data)

		if err != nil {
			fmt.Println("Append Error")
			return err
		}

		err = p.Database.Set(ctx, email, jsons, 0).Err()
		if err != nil {
			fmt.Println("Database Set Error")
			return err
		}
	}

	return nil
}

func (p *ProductDatabase) EmptyProduct(email string) error {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	srt := p.Database.Get(ctx, email)
	if srt.Err() == redis.Nil {
		return errors.New("you dont have anything to remove")
	} else {
		_, err := p.Database.Do(ctx, "DEL", email).Result()
		if err != nil {
			return err
		}
	}
	return nil
}
