package modelss

type Product struct {
	Name     string `json:"name"`
	Price    int    `json:"price"`
	Overview string `json:"overview"`
}

type Products struct {
	AllProds []Product
}
