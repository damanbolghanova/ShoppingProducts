module n_products

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis/v8 v8.7.1
	github.com/golang/protobuf v1.4.3
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
